/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task1;

import java.util.*;

/**
 *
 * @author User-Admin
 */
public class Task1 {

    private static Scanner input = new Scanner(System.in);
    public static int[] arrayA;
    public static int[] arrayB;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       arrayA = uniqueNumbers(ArrayA());
       arrayB = uniqueNumbers(ArrayB());
       //menu display
       System.out.println("Choose 1: Display Numbers\r\n"
                          +"Choose 2: Maximum and Minimum of Numbers\r\n"
                          +"Choose 3: Display the average\r\n"
                          +"Choose 4: Check overlapping\r\n"
                          +"Choose 5: Quit");
        int menu = 0;
        while (menu !=5){
            System.out.println("Choose an option");
            menu = input.nextInt();
            //user has to input 1-5
            
        switch(menu)
        {case 1:
            Display();//displays printed numbers
        break;
        case 2:
        System.out.printf("Your smallest number for A is %d\r\n", MinA());
        System.out.printf("Your largest number for A is %d\r\n", MaxA());
        System.out.printf("Your smallest number for B is %d\r\n", MinB());
        System.out.printf("Your largest number for B is %d\r\n", MaxB());
        //displays largest and smallest numbers of both sets
            break;
        case 3:
            Average();// displays averages
            break;
        case 4:
            if (Overlap( MinB(), MaxB(), MinA(),MaxA())){
                System.out.println("There is an overlap");
            } else {
                System.out.println("There is no overlap");
            }
            //displays if there is overlapping
            break;
        case 5:System.out.println("The Program is Ending");
            break;//quits
        
        default:
            System.out.println("number not recognised!!");
        }
        }
        
        //MaxB();
        //Overlap(MinB(), MaxB(), MinA(), MaxA());
    }

    public static int ArrayA() {
//array for 1st set of numbers
        System.out.println("Please enter the number size of set A");
        int sizeofarrayA;
        sizeofarrayA = input.nextInt();
        return sizeofarrayA;
    }

    public static int ArrayB() {
//array for 2nd set of numbers
        System.out.println("Please enter the number size of set B");
        int sizeofarrayB;
        sizeofarrayB = input.nextInt();
        return sizeofarrayB;
    }

    public static void Numbers() {
        //user to enter numbers
        int i;
        int j;
        for (i = 0; i < arrayA.length; i++) {
            System.out.println("please enter number A");
            arrayA[i] = input.nextInt();
        }
        {
        //user to enter 2nd set of numbers
            for (j = 0; j < arrayB.length; j++) {
                System.out.println("please enter number B");
                arrayB[j] = input.nextInt();
            }
        }
    }
    public static void Display() {//displays numbers 
        int i;
        int j;
        System.out.print("The numbers printed in A are:\r\n");
        for (i = 0; i < arrayA.length; i++){
            System.out.println(arrayA[i]);
        }
       System.out.print("The numbers printed in B are:\r\n");
        for (j = 0; j < arrayB.length; j++){
            System.out.println(arrayB[j]);
        }
}

    public static int MaxA() {//works out largest numbers of set A
        int largestA = arrayA[0];
        for (int i = 1; i < arrayA.length; i++) {
            if (arrayA[i] > largestA) {
                largestA = arrayA[i];
            }
        }
        return largestA;

    }

    public static int MaxB() {//works out largest numbers of set B
        int largestB = arrayB[0];
        for (int i = 1; i < arrayB.length; i++) {
            if (arrayB[i] > largestB) {
                largestB = arrayB[i];
            }
        }

        return largestB;

    }

    public static int MinA() {//works out smallest numbers of set A
        int smallestA = arrayA[0];
        for (int i = 1; i < arrayA.length; i++) {
            if (arrayA[i] < smallestA) {
                smallestA = arrayA[i];
            }
        }

        return smallestA;
    }

    public static int MinB() {//works out smallest numbers of set B
        int smallestB = arrayB[0];
        for (int i = 1; i < arrayB.length; i++) {
            if (arrayB[i] < smallestB) {
                smallestB = arrayB[i];
            }
        }

        return smallestB;

    }
    public static void Average()
    {//works out average of A
        double sumA = 0;
        for(int i=0;i<arrayA.length;i++)
        {
         sumA += arrayA[i];   
        }
        
        double averageA = sumA/ arrayA.length;
        //works out average of B
        double sumB = 0;
        for(int i=0;i<arrayB.length;i++)
        {
         sumB += arrayB[i];    
        }
        
        double averageB = sumB / arrayB.length;
        System.out.printf("Average of A is %.2f and average of B is %.2f\r\n",averageA,averageB);
    }

    public static boolean Overlap(int smallestB, int largestB, int smallestA, int largestA) {
        //System.out.println(smallestB + " + " + largestB + " + " + smallestA + " + " + largestA);
        //checked numbers as always stating there was an overlap!! 
        if ((largestA >= smallestB) && smallestA <= largestB) {
            return true;//checks for overlapping

        } else {
            return false;
        }

    }
    public static int [] uniqueNumbers(int numberquantity)
    {    //creating an int array
        int[] numberlist = new int [numberquantity];
        for (int i=0;i<numberquantity;i++)
        {
           System.out.println("Please insert a unique number");
           //user enters number
            int newNumber = input.nextInt();
            //checking if numnber is unique from array
        if (duplicatecheck(numberlist,i,newNumber))
        {
            //tells user, number is not unique.
            System.out.printf("Error:Number is duplicated. Try Again.\r\n",newNumber);
            i--;
        }
        
        else
        {
            //writes number to array
            numberlist[i] = newNumber;
        }
        
    }
        return numberlist;
    }
        public static boolean duplicatecheck(int[]array,int arraysize,int newNumber)
       {
           //loop through array
            for (int i=0;i<arraysize;i++)
            {//checking array
                if(array[i] == newNumber)
                {
                    return true;
                }
            }
            return false;
        }

}
